using System.Web.Http;
using WebActivatorEx;
using LinkedListAPI;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace LinkedListAPI
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                       
                        c.SingleApiVersion("v1", "LinkedListAPI");
                    })
                .EnableSwaggerUi(c =>
                    {
                        c.DocumentTitle("LinkedList Swagger UI");
                        c.DocExpansion(DocExpansion.List);
                    });
        }
    }
}
