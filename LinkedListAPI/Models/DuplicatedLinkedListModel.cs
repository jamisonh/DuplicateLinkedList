﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace LinkedListAPI.Models
{
    [DataContract]
    public class DuplicatedLinkedListModel
    {
        [DataMember]
        public List<LinkedListNodeModel> OriginalLinkedList { get; set; }

        [DataMember]
        public List<LinkedListNodeModel> DuplicatedLinkedList { get; set; }

        public DuplicatedLinkedListModel()
        {
        }

        public DuplicatedLinkedListModel(List<LinkedListNodeModel> original, List<LinkedListNodeModel> duplicate)
        {
            OriginalLinkedList = original;
            DuplicatedLinkedList = duplicate;
        }
    }
}