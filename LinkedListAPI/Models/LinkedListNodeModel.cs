﻿using System.Runtime.Serialization;

using LibLinkedList;

namespace LinkedListAPI.Models
{
    [DataContract]
    public class LinkedListNodeModel
    {
        [DataMember]
        public string UniqueObjectId { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public string NextTag { get; set; }
        [DataMember]
        public string ReferenceTag { get; set; }

        public LinkedListNodeModel(Node node)
        {
            UniqueObjectId = node.ObjId;
            Tag = node.Tag;
            if (node.Next != null)
            {
                NextTag = node.Next.Tag;
            }
            ReferenceTag = node.Reference.Tag;
        }
    }
}