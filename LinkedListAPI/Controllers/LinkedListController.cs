﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using LibLinkedList;
using LinkedListAPI.Models;

namespace LinkedListAPI.Controllers
{
    public class LinkedListController : ApiController
    {


        [HttpGet]
        [Route("api/linkedlist/generate/{listLength}")]
        [ResponseType(typeof(List<LinkedListNodeModel>))]
        public HttpResponseMessage GenerateList(uint listLength)
        {
            return Request.CreateResponse(
                CircularLinkedListToSerializableList(NodeListGenerator.Generate((int) listLength)));
        }

        [HttpGet]
        [Route("api/linkedlist/generate/duplicate/{listLength}")]
        [ResponseType(typeof(List<LinkedListNodeModel>))]
        public HttpResponseMessage GenerateDuplicateList(uint listLength)
        {
            var originalLinkedList = NodeListGenerator.Generate((int) listLength);
            var duplicatedLinkedList = Node.DuplicateList(originalLinkedList);

            var duplicatedListModel = new DuplicatedLinkedListModel
            {
                OriginalLinkedList = CircularLinkedListToSerializableList(originalLinkedList),
                DuplicatedLinkedList = CircularLinkedListToSerializableList(duplicatedLinkedList)
            };

            return Request.CreateResponse(duplicatedListModel);
        }

        
        private static List<LinkedListNodeModel> CircularLinkedListToSerializableList(Node node)
        {
            var serializableList = new List<LinkedListNodeModel>();
            while (node.Next != null)
            {
                serializableList.Add(new LinkedListNodeModel(node));
                node = node.Next;
            }
            serializableList.Add(new LinkedListNodeModel(node));


            return serializableList;
        }
    }
}
