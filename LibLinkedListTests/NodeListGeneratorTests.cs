﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace LibLinkedList.Tests
{
    [TestClass()]
    public class NodeListGeneratorTests
    {
        [TestMethod]
        public void TestGeneratorLength()
        {
            var expectedNodes = 10;
            var node = NodeListGenerator.Generate(expectedNodes);
            var nodeCount = 1;
            while (node.Next != null)
            {
                nodeCount++;
                node = node.Next;
            }
            Assert.AreEqual(expectedNodes, nodeCount);
        }


        [TestMethod]
        public void TestAllRefs()
        {
            var node = NodeListGenerator.Generate(10000);
            while (node.Next != null)
            {
                Assert.IsNotNull(node.Reference);
                Assert.AreNotSame(node, node.Next);
                node = node.Next;
            }
            //check the last node
            Assert.IsNotNull(node.Reference);
            Assert.IsNull(node.Next);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestMaxInt()
        {
            NodeListGenerator.Generate(NodeListGenerator.MaxNodes + 1);
            
        }
    }
}