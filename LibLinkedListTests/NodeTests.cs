﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LibLinkedList.Tests
{
    [TestClass()]
    public class NodeTests
    {
        [TestMethod()]
        public void DuplicateListTest()
        {
            for (var i = 0; i < 100; i++)
            {
                var node = NodeListGenerator.Generate(10000);
                var clonedNode = Node.DuplicateList(node);
                while (node.Next != null)
                {
                    Assert.AreEqual(node.Tag, clonedNode.Tag);
                    Assert.AreEqual(node.Reference.Tag, clonedNode.Reference.Tag);
                    Assert.AreNotSame(node, clonedNode);
                    Assert.AreNotSame(node.Reference, clonedNode.Reference);
                    node = node.Next;
                    clonedNode = clonedNode.Next;
                }
                Assert.AreEqual(node.Tag, clonedNode.Tag);
                Assert.AreEqual(node.Reference.Tag, clonedNode.Reference.Tag);
                Assert.AreNotSame(node, clonedNode);
                Assert.AreNotSame(node.Reference, clonedNode.Reference);
            }
        }

        [TestMethod]
        public void DuplicateMaxNodesTest()
        {
            var original = NodeListGenerator.Generate(NodeListGenerator.MaxNodes);
            var copy = Node.DuplicateList(original);

            Assert.AreEqual(original.Tag, copy.Tag);
        }
    }
}