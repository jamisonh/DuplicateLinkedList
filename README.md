# Duplicate Linked List

Web API 2.0 API and library to generate a linked list which has a random reference to any node within the list, and then perform a duplication of that original list such that the new list has no dependency on the first.

Swagger UI (via Swashbuckle) is included for interacting with the API.

## Endpoints

|Route   |Description   |
|:-:|---|
|api/linkedlist/generate/{listLength}   | Generates a linked list with a tag, and a random references within the list  |
|api/linkedlist/generate/duplicate/{listLength}   |Same as Generate above, except it creates a complete copy of the list.  Includes a Unique Object ID for each Node to prove they are not in fact the same underlying object.   |

## Running

You can run this solution in Visual Studio, in Docker For Windows with a prebuilt Docker image, or by building the docker image.  Instructions for doing all 3 are below.

> This application was built using Visual Studio 2015.

Application is setup for the default route (`/`) to be redirected to Swagger UI for interacting with the API.

### Visual Studio (Debug)

  1. Open the solution in visual studio
  1. Set the startup project as `LinkedListAPI` (should be already)
  1. Restore nuget packages (if necessary)
  1. Run in debug mode
     1. Browser should open to the default route `/` and get redirected to swagger UI.

### Docker For Windows

You'll need to install Docker on Windows Server 2016 or Windows 10 and "Switch to Windows containers" in it's context menu.

Also of note - Windows Containers are very large compared to linux - Apline Linux is 6MB, Ubuntu is around 300MB, while a stripped down windows server core with IIS and asp.net installed is over 7 GB.

> Docker on windows currently has a bug that prevents you from accessing the running container on `localhost`.  This requires you to run a `docker inspect {container ID}` and use the IP Address assigned by docker (probably 172.something)

#### Running Pre-built Docker Image

Running the following command in PowerShell (from any directory) will start the container and print the assigned IP Address:

> Warning: The following will take awhile...

``` powershell
docker inspect -f "{{ .NetworkSettings.Networks.nat.IPAddress }}" $(docker run --rm -d -p 8081:80 jamisonhyatt/linkedlist)
```

Navigate to the output IP address to interact with Swagger UI

##### Building, Compiling and Running in Docker

These steps will mount the source code into an ephemeral build container that contains `MSBuild` and `nuget`.  Once started, the binaries will be built from the source code and copied into another Windows Server image, with both asp.net and IIS installed.

> Warning - This will take longer than the prebuilt image, as it will need to download the image used for building, as well as the base image for the application.

In PowerShell from the solution directory run...

``` powershell
docker build -t linkedlist .
docker inspect -f "{{ .NetworkSettings.Networks.nat.IPAddress }}" $(docker run --rm -p 8081:80 -d linkedlist)
```

Navigate to the output IP address to interact with Swagger UI