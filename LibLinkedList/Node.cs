﻿using System;

namespace LibLinkedList
{
    /// <summary>
    /// Represents a Node in a linked list, with a Tag representing that data and 
    /// a Reference which is a random reference to some existing node in the list
    /// </summary>
    public class Node
    {
        /// <summary>
        /// This Nodes unique object identifier - Used for proving separate objects
        /// </summary>
        public readonly string ObjId = Guid.NewGuid().ToString();

        /// <summary>
        /// The next node in the linked list
        /// </summary>
        public Node Next { get; set; }

        /// <summary>
        /// The random reference to another node in the linked list.
        /// </summary>
        public Node Reference { get; set; }

        /// <summary>
        /// Returns this nodes tag, represents the data inside the node.
        /// </summary>
        public string Tag { get; }

        /// <summary>
        /// Creates a new Node, with the provided "data" where the "data" is represented by the node Tag.
        /// </summary>
        /// <param name="tag"></param>
        public Node(string tag)
        {
            Tag = tag;
        }

        /// <summary>
        /// Duplicates the input LinkedList such that it has no dependency on the first.
        /// Copies tags and moves references in the copy so they point to the same position in the list.
        /// </summary>
        /// <param name="original">Head Node of the original list to clone</param>
        /// <returns>A full copy of the original linked list with no dependency on the first</returns>
        public static Node DuplicateList(Node original)
        {
            int originalListLength;
            var copyHead = InterleaveList(original, out originalListLength);
            UpdateCopiedReferences(copyHead, originalListLength);
            SeparateInterleave(original, originalListLength);

            return copyHead;
        }

        /// <summary>
        /// Modifies the input original Linked list such that every other original node has a copy between it and the next node.
        /// This results in a linked list twice as long.  The first node continues to be the original node, while the last node is the
        /// copy of the original linked list last node.
        /// </summary>
        /// <param name="original">The head of the interleaved list</param>
        /// <param name="originalListLength">length of the original list</param>
        /// <returns>The head of the new copy</returns>
        private static Node InterleaveList(Node original, out int originalListLength)
        {
            var copyHead = new Node(original.Tag);

            originalListLength = 0;
            var copy = copyHead;
            // Interleave the 2 lists together, into a single list, making the original head the start, and the last item
            // in the list the last copy.
            while (original.Next != null)
            {
                // Increment our currently unknown list size
                originalListLength++;
                // Make sure we preserve the original next value before we interleave
                var savedOriginalNext = original.Next;

                // Start interleaving the lists, by pointing the current nodes next value to the copy
                original.Next = copy;
                // Pointing the copies reference to the original's reference
                copy.Reference = original.Reference;
                // and pointing the copies next value to the next item in the original list.
                copy.Next = savedOriginalNext;

                // Walk the list
                copy = new Node(savedOriginalNext.Tag);
                original = savedOriginalNext;
            }
            // Last increment and repoint after loop
            originalListLength++;
            original.Next = copy;
            copy.Reference = original.Reference;

            return copyHead;
        }

        /// <summary>
        /// Modifies every copies reference in the input interleaved list to point to the it's pairing in the copied list.
        /// </summary>
        /// <param name="copy">Copy head of the interleaved list to update references for</param>
        /// <param name="originalListLength">The length of the original list</param>
        private static void UpdateCopiedReferences(Node copy, int originalListLength)
        {
            // Loops through every copy, by skipping the original
            for (var i = 1; i <= originalListLength; i++)
            {
                // This sets the current copy reference to it's correct place on the copied list
                // by using the interleave; the random reference value (on the original list) has a next 
                // value which points to the original point on the list.
                copy.Reference = copy.Reference.Next;

                // If its not the end of the list, Walk to next copy, through original 
                if (i < originalListLength)
                    copy = copy.Next.Next;
            }
        }

        /// <summary>
        /// Undoes the previously applied interleave of the original and copied lists
        /// </summary>
        /// <param name="original">The head of the interleaved list</param>
        /// <param name="originalListLength">length of the original list</param>
        private static void SeparateInterleave(Node original, int originalListLength)
        {
            var copy = original.Next;
            for (var i = 1; i <= originalListLength; i++)
            {
                // If it's not the last item in the list.....
                if (i < originalListLength)
                {
                    // Since copy.Next points to the original.Next value, save it before modification.
                    var preservedNext = copy.Next;

                    // Separate the interleave back into 2 separate lists
                    // copy.Next is an item in the original list whose next value points to the copy we want
                    copy.Next = copy.Next.Next;
                    // repoint the original's next reference to the preserved next
                    original.Next = preservedNext;

                    // Walk both original and copy forward
                    original = original.Next;
                    copy = copy.Next;
                }
                else // It is the last node in each respective list, set the next values to null.
                {
                    original.Next = null;
                    copy.Next = null;
                }
            }
        }

    }
}
